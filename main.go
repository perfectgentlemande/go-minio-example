package main

import (
	"crypto/tls"
	"github.com/go-chi/chi"
	"github.com/minio/minio-go"
	"io/ioutil"
	"log"
	"mime/multipart"
	"net/http"
)

type FileController struct {
	minioClient *minio.Client
	bucketName string
}

func (fc *FileController) getFile(w http.ResponseWriter, r *http.Request) {
	fileName := chi.URLParam(r, "file_name")

	log.Println(fileName)

	reader, err := fc.minioClient.GetObjectWithContext(r.Context(), fc.bucketName, fileName, minio.GetObjectOptions{})
	if err != nil {
		log.Fatal("Cannot get object from minio",err)
	}
	defer reader.Close()

	objInfo, err := reader.Stat()
	if err != nil {
		log.Fatal("Cannot get reader stats",err)
	}

	fileBytes, err := ioutil.ReadAll(reader)
	if err != nil {
		log.Fatal("Cannot read",err)
	}



	w.Header().Set("Content-Type", objInfo.ContentType)
	w.WriteHeader(http.StatusOK)
	w.Write(fileBytes)
}

func (fc *FileController) deleteFile(w http.ResponseWriter, r *http.Request) {
	fileName := chi.URLParam(r, "file_name")

	log.Println(fileName)

	err := fc.minioClient.RemoveObject(fc.bucketName, fileName)
	if err != nil {
		log.Fatal("Cannot remove object from s3",err)
	}
}


func (fc *FileController) createFile(w http.ResponseWriter, r *http.Request) {
	const maxMemory = 50 << 20 // 50 MiB

	err := r.ParseMultipartForm(maxMemory)
	if err != nil {
		if err == multipart.ErrMessageTooLarge {
			log.Fatal("file too large",err)
		}

		log.Fatal("cannot parse multipart form: ", err)
	}

	file, header, err := r.FormFile("data")
	if err != nil {
		log.Fatal("cannot form file: ", err)
	}

	_, err = fc.minioClient.PutObjectWithContext(r.Context(), fc.bucketName, header.Filename, file, header.Size,  minio.PutObjectOptions{ContentType:header.Header.Get("Content-Type")})
	if err != nil {
		log.Fatal("Cannot add object to minio",err)
	}
}

func main() {

	// S3 connect
	bucketName := "oebs-sftp"
	minioServer := "your server"
	accessKey := "your access key"
	secretKey := "your secret key"

	s3Client, err := minio.New(minioServer, accessKey, secretKey, true)
	if err != nil {
		log.Fatal("failed to create minio")
	}
	tr := &http.Transport{
		TLSClientConfig:    &tls.Config{InsecureSkipVerify: true},
		DisableCompression: true,
	}
	s3Client.SetCustomTransport(tr)

	fileController := FileController{
		minioClient: s3Client,
		bucketName: bucketName,
	}

	r := chi.NewRouter()

	r.Get("/{file_name}", fileController.getFile)
	r.Post("/", fileController.createFile)
	r.Delete("/{file_name}", fileController.deleteFile)

	http.ListenAndServe(":3000", r)

}